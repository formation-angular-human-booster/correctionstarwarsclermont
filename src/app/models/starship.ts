export class Starship {
  id: number;
  name: string;
  model: string;
  cost: number;
  constructor(id: number, name: string, model: string, cost: number) {
    this.id = id;
    this.name = name;
    this.model = model;
    this.cost = cost;
  }
}
