export class Planet {
  id: number;
  name: string;
  climat: string;
  terrain: string;
  created: Date;
  constructor(id: number, name: string, climat: string, terrain: string, created: Date) {
    this.id = id;
    this.name = name;
    this.climat = climat;
    this.terrain = terrain;
    this.created = created;
  }
}
