import {Component, Input, OnInit} from '@angular/core';
import {Starship} from '../../models/starship';

@Component({
  selector: 'app-starship-details',
  templateUrl: './starship-details.component.html',
  styleUrls: ['./starship-details.component.css']
})
export class StarshipDetailsComponent implements OnInit {
  @Input() starship: Starship;
  constructor() { }

  ngOnInit() {
  }

}
