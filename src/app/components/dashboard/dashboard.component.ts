import { Component, OnInit } from '@angular/core';
import {Planet} from '../../models/planet';
import {Starship} from '../../models/starship';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  planets: Planet[];
  starships: Starship[];
  planetSelected: Planet;
  starshipSelected: Starship;

  constructor() {
    this.planets = [
      new Planet(1, 'Pluton', 'Chaud', 'Sec',
        new Date(1901, 12, 12)),
      new Planet(2, 'Mars', 'Froid', 'Boueux',
        new Date(1993,3,30))
    ];
    this.starships = [
      new Starship(1, 'Clio', 'Renault', 10000),
      new Starship(2, 'Serie 1', 'BMW', 30000)
    ];
  }

  ngOnInit() {
  }

  changePlanet(planet: Planet) {
    this.planetSelected = planet;
    this.starshipSelected = null;
  }

  changeStarship(starship: Starship) {
    this.starshipSelected = starship;
    this.planetSelected = null;
  }
}
