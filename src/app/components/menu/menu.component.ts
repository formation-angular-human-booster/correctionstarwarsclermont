import { Component, OnInit } from '@angular/core';
import {faHome} from '@fortawesome/free-solid-svg-icons/faHome';
import {faTruckMonster} from '@fortawesome/free-solid-svg-icons/faTruckMonster';
import {faGlobe} from '@fortawesome/free-solid-svg-icons/faGlobe';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  faHome = faHome;
  faTruck = faTruckMonster;
  faGlobe = faGlobe;
  constructor() { }

  ngOnInit() {
  }

}
